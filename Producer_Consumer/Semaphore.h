#include <mutex>
#include <condition_variable>
using namespace std;

#pragma once
class Semaphore
{
public:
	Semaphore(void);
	Semaphore (int available);
	~Semaphore(void);
	void notify();
	void wait();
	int remaining();
private:
	mutex myMutex;
	condition_variable conV;
	unsigned int count;

};

