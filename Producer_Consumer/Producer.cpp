#include "Producer.h"
#include <iostream>


using namespace std;


Producer::Producer(void)
{
}

Producer::Producer(int producerID){
	this->producerID = producerID;
}

Producer::~Producer(void)
{
}

Material* Producer::makeMaterial(){

	//if queue is full
		//sleep until available
	//else
	Material* mat = new Material(producerID);
	//cout << "Producer " << producerID << " has just created a material." << endl;
	return mat;
}

int Producer::getID(){
	return producerID;
}