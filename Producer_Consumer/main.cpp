#include <string>
#include <vector>
#include <time.h>
#include <iostream>
#include <thread>
#include <chrono>
#include <mutex>
#include <condition_variable>
#include "Material.h"
#include "Producer.h"
#include "Queue.h"
#include "Semaphore.h"
using namespace std;

//global mutexes, CVs, and buffer counts
mutex prodAMutex; //those mutexes
mutex prodBMutex;
mutex prodCMutex;
mutex displayMutex;
mutex processorMutex;
mutex topMutex;
mutex toolMutex;
mutex matSearchMutex;
mutex pausing;
condition_variable aCV; //condition variables that triggers sleep and wake
condition_variable bCV;
condition_variable cCV;
condition_variable materialCV;
condition_variable toolsCV;
condition_variable pausingCV;


int bufferA = 0; //inputBuffers
int bufferB = 0;
int bufferC = 0;
int ABCount = 0; //output products
int BCCount = 0;
int ACCount = 0;
int tools = 3;
vector<thread> VectorThreads;
bool suspend = false;

string outputLane = "";
string last = "";

chrono::milliseconds producingT(1000); //time it takes for a producer to produce one material

void producer(Producer ID);
void processor(int i);
void displayBuffer(int buffer, int ID);
void UI();
void makeProduct(int i);
void getTools(int i);
void getMaterials(int whatToMake);
void returnMats(int whatToMake);




int main(){
	
	
	//Initializing Producers
	Producer prod1(1);
	Producer prod2(2);
	Producer prod3(3);

	VectorThreads.push_back(thread(producer,prod1)); // producerA
	VectorThreads.push_back(thread(producer,prod2)); // producerB
	VectorThreads.push_back(thread(producer,prod3)); // producerC
	//VectorThreads.push_back(thread(processor));
	//VectorThreads.push_back(thread(processor));
	//VectorThreads.push_back(thread(processor));
	thread p1(processor,1);
	thread p2(processor,2);
	thread p3(processor,3);
	//thread p4(processor,4);
	thread tUI(UI);
	tUI.detach();
	/*t1.detach();
	t2.detach();
	t3.detach();*/
	p1.detach();
	p2.detach();
	p3.detach();
	//p4.detach();
	
	while(true){//forever running
	
	}
	
	return 0;
}

void producer(Producer ID){

	while (true){
		while (suspend)
			continue;
		if (ID.getID() ==1){
			unique_lock<mutex> locker(prodAMutex);
			if (bufferA == 10) aCV.wait(locker);
			bufferA++;
			if (bufferA == 1) aCV.notify_one();
		}		
		else if (ID.getID() == 2){
			unique_lock<mutex> locker(prodBMutex);
			if (bufferB == 10) bCV.wait(locker);
			bufferB++;
			if (bufferB == 1) bCV.notify_one();
		}
		else if (ID.getID() == 3){
			unique_lock<mutex> locker(prodCMutex);
			if (bufferC == 10) cCV.wait(locker);
			bufferC++;
			if (bufferC == 1) cCV.notify_one();
		}
		this_thread::sleep_for(producingT);
	}
	
}


void displayBuffer(int buffer, int ID){
	if (ID == 1) cout << "A's buffer: ";
	else if (ID == 2) cout << "B's buffer: ";
	else if (ID == 3) cout << "C's buffer: ";
	int i = 1;
	while (i <= buffer){
		cout << "X";
		i++;
	}
	while (i <= 10){
		cout << "-";
		i++;
	}
	cout <<endl;
}

void UI(){
	while (true){
		
		int i;
		cout << "What is your choice?" << endl;
		cout << "(1) to display all buffers." << endl;
		cout << "(2) to display current output buffer." << endl;
		cout << "(3) to display the current number of each product." <<endl;
		cout << "(4) adjust operator #." << endl;
		cout << "(5) adjust tools #." << endl;
		cout << "(6) display the current operator #, tool #." << endl;
		cout << "(7) pause all threads." << endl;
		cout << "(8) pause all threads." << endl;
		cin >> i;

		if (i == 1){
			displayBuffer(bufferA,1);
			displayBuffer(bufferB,2);
			displayBuffer(bufferC,3);
		}
		if (i == 2){
			cout << outputLane << endl;
		}
		if (i == 3){
			cout << "Product [AB] : " << ABCount << endl;
			cout << "Product [BC] : " << BCCount << endl;
			cout << "Product [AC] : " << ACCount << endl;
		}
		if (i == 4){
			int i;
			cout << "Provide a number to increase/decrease: " << endl;
			cin >> i;
			if ((int)VectorThreads.size() -3 + i < 0)
				cout << "Error: The number received exceeds the number of actual operators at work." << endl;
			else if (i>=0){
				for (int k=0;k<i;k++)
					VectorThreads.push_back(thread(processor,99));
			}
			else if (i<0){
				for (int k=0;k>i;k--)
					VectorThreads.pop_back();
			}
			cout << "Current # of operators : " << VectorThreads.size() - 3 << endl;
		}
		if (i == 5){
			unique_lock<mutex> halt(toolMutex);
			int i;
			cout << "Provide a number to increase/decrease: " << endl;
			cin >> i;
			if (tools + i < 0)
				cout << "Error: Can't have negative number of tools." << endl;
			else 
				tools = tools + i;
			cout << "Current # of tools : " << tools << endl;}
		if (i == 6){
			cout << "Current # of operators : " << VectorThreads.size() - 3 << endl;
			cout << "Current # of tools : " << tools << endl;
		}
		if (i == 7){
			cout << "Pausing all threads..." << endl;
			suspend = true;
		}
		if (i == 8){
			cout << "Resuming all threads..." << endl;
			suspend = false;
		}

	}

}

void processor(int i){
	srand(time(NULL));
	
	while (true){ //forever running
		while (suspend)
			continue;

		bool hasTools = false;
		bool productMade = false;
		while (productMade == false){ //while product has yet to be made.
			int whatToMake;
			string recordedLast = last;
			if (recordedLast.compare("|AB|") == 0){ // if last was AB
				if (abs(BCCount-ACCount) <9)
					whatToMake = (rand() % 10 + 0 )< 5? 2 : 3;
				else whatToMake = BCCount < ACCount ? 2 : 3;
			}
			else if(recordedLast.compare("|BC|")==0){ // if last was BC
				if (abs(ABCount-ACCount) <9)
					whatToMake = (rand() % 10 + 0 )< 5? 1 : 3;
				else whatToMake = ABCount < ACCount ? 1 : 3;
			}
			else if (recordedLast.compare("|AC|")==0){
				if (abs(ABCount-BCCount) <9)
					whatToMake = (rand() % 10 + 0 )< 5? 1 : 2;
				else whatToMake = ABCount < BCCount ? 1 : 2;
			}
			else whatToMake = rand() % 3 + 1;

		
			if (hasTools == false){
				thread matT(getMaterials,whatToMake);
				thread toolT(getTools,i);
				toolT.join();			
				matT.join();
				hasTools = true;
			}
			else{
				thread matT(getMaterials,whatToMake);
				matT.join();
			}


			cout<<"Testing..." << endl;
			cout <<"Recorded Last : " << recordedLast << ". Own's Last : " << last << "." << endl;
		

		
			if (recordedLast.compare(last) != 0){
				returnMats(whatToMake);
				cout << "Here?" << endl;
				//unique_lock<mutex> locker(toolMutex); 
				//tools = tools + 2;
				cout << i << " Materials returned for switch." << endl;
				//if (tools >=2) toolsCV.notify_one();
				continue;
			}
			else {
				makeProduct(whatToMake);
				unique_lock<mutex> locker(toolMutex); 
				tools = tools + 2;
				cout << i << " Operator Product made. Tools returned. Tools now:" << tools << endl;
				productMade = true;
				if (tools >=2)  toolsCV.notify_one();
			}
			
		//done processing
		
	}

	
	}	

}



void makeProduct(int i){
	unique_lock<mutex> mainMatLock(matSearchMutex); 
	if (i == 1){
		last = "|AB|";
		outputLane.append(last);
		ABCount++;
	}
	if (i==2){
		last = "|BC|";
		outputLane.append(last);
		BCCount++;
	}
	if (i==3){
		last = "|AC|";
		outputLane.append(last);
		ACCount++;
	}
	this_thread::sleep_for(chrono::milliseconds(rand() % 1000 + 10));
	
}
	
void getTools(int i){
	unique_lock<mutex> locker(toolMutex); 
	if (tools <2){
		cout << i << " Waiting on tools..." << endl;
		toolsCV.wait(locker);}
	tools = tools - 2;//tools acquired
	cout << i <<" Tools taken. Remaining: " << tools << endl;
	
}

void getMaterials(int whatToMake){
	if (whatToMake == 1){
		unique_lock<mutex> locking(prodAMutex);
		while (bufferA<1) aCV.wait(locking);
		bufferA--;
		aCV.notify_one();
		locking = unique_lock<mutex>(prodBMutex);
		while (bufferB<1) bCV.wait(locking);
		bufferB--;
		bCV.notify_one();
	}
	else if (whatToMake == 2){
		unique_lock<mutex> locking(prodBMutex);
		while (bufferB<1) bCV.wait(locking);
		bufferB--;
		bCV.notify_one();
		locking = unique_lock<mutex>(prodCMutex);
		while (bufferC<1) cCV.wait(locking);
		bufferC--;
		cCV.notify_one();
	}
	else if (whatToMake == 3){
		unique_lock<mutex> locking(prodAMutex);
		while (bufferA<1) aCV.wait(locking);
		bufferA--;
		aCV.notify_one();
		locking = unique_lock<mutex>(prodCMutex);
		while (bufferC<1) cCV.wait(locking);
		bufferC--;
		cCV.notify_one();
	}
}

void returnMats(int whatToMake){
	if (whatToMake == 1){
		unique_lock<mutex> locking(prodAMutex);
		if (bufferA<10) {bufferA++;}
		aCV.notify_one();
		locking = unique_lock<mutex>(prodBMutex);
		if (bufferB<10) {bufferB++;}
		bufferB++;
		bCV.notify_one();
	}
	else if (whatToMake == 2){
		unique_lock<mutex> locking(prodBMutex);
		if (bufferB<10) {bufferB++;}
		bufferB++;
		bCV.notify_one();
		locking = unique_lock<mutex>(prodCMutex);
		if (bufferC<10) {bufferC++;}
		bufferC++;
		cCV.notify_one();
	}
	else if (whatToMake == 3){
		unique_lock<mutex> locking(prodAMutex);
		if (bufferA<10) {bufferA++;}
		bufferA++;
		aCV.notify_one();
		locking = unique_lock<mutex>(prodCMutex);
		if (bufferC<10) {bufferC++;}
		bufferC++;
		cCV.notify_one();
	}

}
