#include <iostream>
#include "Queue.h"

using namespace std;

Queue::Queue(void)
{
	frontnode = 0;
	rearnode = 0;
	csize = 0;
}


Queue::Queue(int n)
{
	frontnode = 0;
	rearnode = 0;
	csize = 0;
	maxsize = n;
}


Queue::~Queue(void)
{
}


int Queue::size(){
	return csize;
}


bool Queue::empty(){
	return (frontnode == 0 && rearnode==0);
}


bool Queue::full(){
	return (csize == maxsize);
}


void Queue::enqueue(Material* item){
	MaterialNode* tempNode;
	tempNode = new MaterialNode(item, 0);
	if (empty() == true){
		rearnode = tempNode;
		frontnode = tempNode;
	}
	else{
		tempNode->next = rearnode;}
	rearnode = tempNode;
	csize++;
}


Material* Queue::dequeue(){
	MaterialNode* nodeToRemove = frontnode;
	MaterialNode* tempNode = rearnode;
	Material* data = nodeToRemove->nodeMaterial;
	while (tempNode->next!=nodeToRemove)
		tempNode = tempNode->next;
	frontnode = tempNode;
	frontnode->next = NULL;
	delete nodeToRemove;
	csize--;
	return data;

}

void Queue::displayQueue(){
	MaterialNode* pNode = rearnode;
	int leftover = 10;
	while (pNode!= NULL){
		cout << "|" << pNode->nodeMaterial->getMatType() << "|";
		pNode = pNode->next;
		leftover--;
	}
	while (leftover>0){
		cout << "|  |";
		leftover--;
	}
	cout << endl;
	delete pNode;

}