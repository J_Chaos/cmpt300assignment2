
#include "Material.h"
#pragma once
class Producer
{
public:
	Producer(void);
	Producer(int producerID);
	~Producer(void);
	Material* makeMaterial();
	int getID();
private:
	int producerID;

	
};

