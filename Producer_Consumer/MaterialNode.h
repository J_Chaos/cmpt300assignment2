#include "Material.h"
#pragma once
class MaterialNode
{
public:
	MaterialNode(Material* content, MaterialNode* next_node);
	~MaterialNode(void);
	Material* nodeMaterial;
	MaterialNode* next;
};

