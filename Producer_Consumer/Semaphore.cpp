#include "Semaphore.h"


Semaphore::Semaphore(void)
{
}


Semaphore::~Semaphore(void)
{
}

Semaphore::Semaphore(int available){ //Initialize a semaphore
	count = available;
}

void Semaphore::notify(){
	unique_lock<mutex> locker(myMutex);
	count++;
	conV.notify_one();
}

void Semaphore::wait(){
	unique_lock<mutex> locker(myMutex);
	while (!count)
		conV.wait(locker);
	count--;
}

int Semaphore::remaining(){
	return 10 - count;
}
