#include "MaterialNode.h"
#pragma once

class Queue
{
public:
	Queue(void);
	Queue(int n);
	~Queue(void);
	int size();
	bool empty();
	bool full();
	void enqueue(Material* item);
	Material* dequeue();
	void displayQueue();
private:
	int csize;
	int maxsize;
	MaterialNode* frontnode;
	MaterialNode* rearnode;

};

